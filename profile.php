<?php
include("connection.php");
session_start();
$connect = $con;

if ($_SESSION['IsLoggedin'] == 'true') {
   
    $_SESSION['isFromCheckout'] = 'no';
    $_SESSION['isBackFromOrders'] = 'no'; 

    if (filter_input(INPUT_GET, 'action') == 'logout') {
        $_SESSION['IsLoggedin'] = 'false';
        $_SESSION['UserId'] = '';
        $_SESSION['UserName'] = '';
        header('location: mainpage.php');
    }
} else {
    header('location: mainpage.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist\css\bootstrap.min.css"/>
    <link rel="stylesheet" href="mainpage.css"/>
    <link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css"/>
    <script src="jquery.min.js"></script>
    <script src="bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
    <script src="sweetalert.min.js"></script>
</head>
<body style="background-color:rgb(232,232,232);width:100%">

<!-- more_pop_up -->
    <!-- cusomet care pop up -->
    <div id="myModal_customercare" class="modal fade" role="dialog">
    <div class="modal-dialog">

                    <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Care</h4>
                </div>
                    <div class="modal-body">
                    <p><b class="text-danger">
                        24/7 care
                    </b></p>
                    <i class="fa fa-phone"></i> &nbsp;9739751689<br/>
                    <i class="fa fa-envelope"></i> &nbsp;bmcgroups.scotchhub@gmail.com
                    </div>
                </div>

            </div>

    </div>
    </div>

    <!-- about us pop up -->
        <!-- cusomet care pop up -->
        <div id="myModal_aboutus" class="modal fade" role="dialog">
    <div class="modal-dialog">

                    <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">About Us</h4>
                </div>
                <div class="modal-body">
                    <div class="content">
                    <p>
                        <b>Scotch Hub</b> is an initial sample product of <b>bmch group</b>'s IT sector. 
                    </p>
                    </div>
                </div>

            </div>

    </div>
</div>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" style="margin-left:2em" href="home.php">Scotch Hub</a>
            </div>
        <ul class="nav navbar-nav navbar-right">
            <!--<li class="active"><a href="#">Home</a></li>
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 2</a></li>-->
            <li class="dropdown">
            <a class="dropdown-toggle" href="#">
            <?php echo $_SESSION['UserName']; ?>
                <i class="fa fa-angle-down"></i>
                <i class="fa fa-angle-up"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="orders.php"><i class="fa fa-shopping-bag"></i> &nbsp;My Orders</a></li>
                <!-- <li class="divider"></li>
                <li><a href="#"><i class="fa fa-address-book-o"></i> &nbsp;My Address</a></li> -->
                <li class="divider"></li>
                <li><a href="profile.php"><i class="fa fa-user-circle-o"></i> &nbsp;My Profile</a></li>
                <li class="divider"></li>
                <li><a href="cart.php?action=logout"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Logout</a></li>
            </ul>
            </li>
            <li>
                <a href="cart.php"><i class="fa fa-shopping-cart" style="color:white"></i> &nbsp;Cart
                <?php

                    $uid = $_SESSION['UserId'];
                    $query = "SELECT COUNT(*) FROM cart where UserId = $uid";
                    $result = mysqli_query($connect, $query);
                    $count = mysqli_fetch_array($result);
                    if ($count[0] > 0):
                    ?>
                    <span class="badge" style="font-size:11px">
                    <?php echo $count[0]; ?>
                    </span>
                <?php endif;?>
                </a>
            </li>
            <li class="dropdown" style="margin-right:5em">
            <a class="dropdown-toggle" href="#">More
                <i class="fa fa-angle-down"></i>
                <i class="fa fa-angle-up"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#" data-toggle="modal" data-target="#myModal_customercare"><i class="fa fa-question-circle"></i> &nbsp;Customer Care</a></li>
                <li class="divider"></li>
                <li><a href="#" data-toggle="modal" data-target="#myModal_aboutus"><i class="fa fa-address-book-o"></i> &nbsp;About Us</a></li>
            </ul>
            </li>
        </ul>
        <ul class="navbar-form ">
            <div class="input-group">
            <input type="text" class="form-control" style="height:30px;width:500px;margin-top:3px" placeholder="Search for products" name="search">
            <div class="input-group-btn">
                <a class="btn btn-default" href="home.php" name="navSearch" style="height:30px;margin-top:3px">
                <i class="glyphicon glyphicon-search"></i>
                </a>
            </div>
            </div>
        </ul>
        </div>
    </nav>


    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading"><span style="font-size:20px">My Profile</span></div>
            <div class="panel-body">
            <?php
                $uid = $_SESSION['UserId'];
                $query = "SELECT * FROM user where Id = '$uid'";
                $result = mysqli_query($connect, $query);
                while ($product = mysqli_fetch_assoc($result)) {
            ?>

                <div class="form-group row">
                    <h4 class="text-right col-md-7" style="margin-left:1em">Personal Information</h4>
                    <div class="col-md-2" style="margin-left:6em">
                        <input type="button" name="pinfoEdit" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-info" value="Edit">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Name</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="username" class="form-control" value="<?php echo $product['Name']; ?>" disabled>
                    </div>
                </div>
                    
                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Gender</label>
                    </div>
                    <div class="col-md-5">
                      <div class="form-control">
                      <fieldset disabled>
                        <?php if($product['Gender'] == 'male'){ ?>
                            <input type="radio" name="gender" value="male" checked> Male
                            <input type="radio" name="gender" value="female"> Female
                        <?php } 
                            else{ ?>
                                <input type="radio" name="gender" value="male"> Male
                                <input type="radio" name="gender" value="female" checked> Female
                        <?php } ?>
                        </fieldset>
                      </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Date of birth</label>
                    </div>
                    <div class="col-md-5">
                        <input type="date" name="dob" class="form-control" value="<?php echo $product['DOB']; ?>" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Mobile No</label>
                    </div>
                    <div class="col-md-5">
                        <input type="number" name="mobile" class="form-control" value="<?php echo $product['MobileNo']; ?>" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Email Id</label>
                    </div>
                    <div class="col-md-5">
                        <input type="email" name="email" class="form-control" value="<?php echo $product['EmailId']; ?>" disabled>
                    </div>
                </div>
                <br/>

                <div class="form-group row">
                    <h4 class="text-right col-md-6" style="margin-left:3em">Address</h4>
                    <div class="col-md-2" style="margin-left:10em">
                        <input type="button" name="addressEdit" data-toggle="modal" data-target="#myModal2" class="btn btn-sm btn-info" value="Edit">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">House/ Building Name</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="bname" class="form-control" value="<?php echo $product['Address']; ?>" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Area</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="area" class="form-control" value="<?php echo $product['Area']; ?>" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Landmark</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="landmark" class="form-control" value="<?php echo $product['Landmark']; ?>" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">City</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="city" class="form-control" value="<?php echo $product['City']; ?>" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">State</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="state" class="form-control" value="<?php echo $product['State']; ?>" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top:5px">Pincode</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="pincode" class="form-control" value="<?php echo $product['PinCode']; ?>" disabled>
                    </div>
                </div>

        <!-- Personal info edit Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Personal Information</h4>
			  </div>
			  <div class="modal-body">
				<form method="post" id="pinfoedit_form" name="pinfoedit_form">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" value="<?php echo $product["Name"]; ?>" name="name" id="name" required>
					</div>
					<div class="form-group">
						<label for="gender">Gender</label>
						<div class="form-control">
                        <?php if($product['Gender'] == 'male'){ ?>
                            <input type="radio" name="gender" value="male" checked> Male
                            <input type="radio" name="gender" value="female"> Female
                        <?php } 
                            else{ ?>
                                <input type="radio" name="gender" value="male"> Male
                                <input type="radio" name="gender" value="female" checked> Female
                        <?php } ?>
					</div>
					</div>
					<div class="form-group">
						<label for="dob">Date Of Birth</label>
						<input type="date" class="form-control" value="<?php echo $product["DOB"]; ?>" name="dob" id="dob" required>
					</div>
					<div class="form-group">
						<label for="mno">Mobile Number</label>
						<input type="tel" class="form-control" pattern="[6|7|8|9][0-9]{9}" value="<?php echo $product["MobileNo"]; ?>" name="mno" id="mno" required>
					</div>
					<div class="form-group">
						<label for="eid">Email Id</label>
						<input type="email" class="form-control" value="<?php echo $product["EmailId"]; ?>" name="eid" id="eid" required>
					</div>
					
					<div class="form-group">
						<button type="submit" name="btnSignup" id="btnSignup" class="btn btn-info"
						style="width:7em;margin-left:40%">Update</button> <!-- data-dismiss="modal" data-toggle="modal" data-target="#myModal" -->
					</div>
				</form>
			  </div>
			  <div class="modal-footer">

			  </div>
			</div>

		  </div>
		</div>

        <!--Edit Address Modal -->
		<div id="myModal2" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Your Address</h4>
			  </div>
			  <div class="modal-body">
				<form method="post" id="editaddress_form" name="editaddress_form">
					<div class="form-group">
						<label for="bname2">House/ Building Name</label>
						<input type="text" class="form-control" value="<?php echo $product["Address"]; ?>" name="bname" id="bname2" required>
					</div>
					<div class="form-group">
						<label for="area2">Area</label>
                        <input type="text" class="form-control" value="<?php echo $product["Area"]; ?>" name="area" id="area2" required>
					</div>
					<div class="form-group">
						<label for="landmark2">Landmark</label>
						<input type="text" class="form-control" value="<?php echo $product["Landmark"]; ?>" name="landmark" id="landmark2" required>
					</div>
					<div class="form-group">
						<label for="city2">City</label>
						<input type="text" class="form-control" value="<?php echo $product["City"]; ?>" name="city" id="city2" required>
					</div>
					<div class="form-group">
						<label for="pincode2">Pincode</label>
						<input type="number" class="form-control" value="<?php echo $product["PinCode"]; ?>" name="pincode" id="pincode2" required>
					</div>
					<div class="form-group">
						<label for="state2">State</label>
						<input type="text" class="form-control" value="<?php echo $product["State"]; ?>" name="state" id="state2" required>
					</div>
					<div class="form-group">
						<button type="submit" name="btnUpdateAddress" class="btn btn-info"
						style="width:7em;margin-left:40%">Update</button> <!-- data-dismiss="modal" data-toggle="modal" data-target="#myModal" -->
					</div>
				</form>
			  </div>
			  <div class="modal-footer">
			  </div>
			</div>

		  </div>
		</div>

            <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>

<script>

$(document).ready(function(){

    $('#pinfoedit_form').on('submit', function(e){
    e.preventDefault();
    $.ajax({
        url: "editPinfo.php",
        method: "POST",
        data: $('#pinfoedit_form').serialize(),
        success: function(data)
        {
            if(data == 'saved')
            {
                $('#pinfoedit_form')[0].reset();
                $('#myModal').modal('hide');
                swal({  title: 'Personal Information updated Successfully!',
                        icon: 'success' ,
                }).then(function() {
                window.location = "profile.php";
                });
            }
            else{
                $('#pinfoedit_form')[0].reset();
                $('#myModal').modal('hide');
                swal({  title: 'Something wrong happened!',
                        icon: 'warning' ,
                }).then(function() {
                window.location = "profile.php";
                });
            }
        }
    });
});

    $('#editaddress_form').on('submit', function(e){
    e.preventDefault();
    //alert('aaaa');
    $.ajax({
        url: "addAddress.php",
        method: "POST",
        data: $('#editaddress_form').serialize(),
        success: function(data)
        {
            if(data == 'saved')
            {
                $('#editaddress_form')[0].reset();
                $('#myModal2').modal('hide');
                swal({  title: 'Address updated Successfully!',
                        icon: 'success' ,
                }).then(function() {
                window.location = "profile.php";
                });
            }
            else{
                $('#editaddress_form')[0].reset();
                swal({  title: 'Something wrong happened!',
                        icon: 'warning' ,
                }).then(function() {
                window.location = "profile.php";
                });
            }
        }
    });
    });
});


</script>

</body>
</html>