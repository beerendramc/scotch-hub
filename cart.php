<?php
include("connection.php");
session_start();
$connect = $con;

if ($_SESSION['IsLoggedin'] == 'true') {
   
    $_SESSION['isFromCheckout'] = 'no';
    $_SESSION['isBackFromOrders'] = 'no'; 
    
    if (filter_input(INPUT_GET, 'action') == 'delete') {
        $Id = filter_input(INPUT_GET, 'id');
        $query = "DELETE FROM cart where Id = $Id";
        $result = mysqli_query($connect, $query);
    }

    if (filter_input(INPUT_GET, 'action') == 'logout') {
        $_SESSION['IsLoggedin'] = 'false';
        $_SESSION['UserId'] = '';
        $_SESSION['UserName'] = '';
        header('location: mainpage.php');
    }
} else {
    header('location: mainpage.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Cart</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist\css\bootstrap.min.css"/>
    <link rel="stylesheet" href="mainpage.css"/>
    <link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css"/>
    <script src="jquery.min.js"></script>
    <script src="bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
    <script src="sweetalert.min.js"></script>
</head>
<body style="background-color:rgb(232,232,232);width:100%">

<!-- more_pop_up -->
		  <!-- cusomet care pop up -->
          <div id="myModal_customercare" class="modal fade" role="dialog">
		  <div class="modal-dialog">

							<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Customer Care</h4>
						</div>
							<div class="modal-body">
                            <p><b class="text-danger">
										24/7 care
									</b></p>
									<i class="fa fa-phone"></i> &nbsp;9739751689<br/>
									<i class="fa fa-envelope"></i> &nbsp;bmcgroups.scotchhub@gmail.com
							</div>
						</div>

					</div>

		  </div>
			</div>

			<!-- about us pop up -->
			 <!-- cusomet care pop up -->
			 <div id="myModal_aboutus" class="modal fade" role="dialog">
		  <div class="modal-dialog">

							<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">About Us</h4>
						</div>
						<div class="modal-body">
							<div class="content">
                                <p>
									<b>Scotch Hub</b> is an initial sample product of <b>bmch group</b>'s IT sector. 
								</p>
							</div>
						</div>

					</div>

		  </div>
	  	</div>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" style="margin-left:2em" href="home.php">Scotch Hub</a>
            </div>
        <ul class="nav navbar-nav navbar-right">
            <!--<li class="active"><a href="#">Home</a></li>
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 2</a></li>-->
            <li class="dropdown">
            <a class="dropdown-toggle" href="#">
            <?php echo $_SESSION['UserName']; ?>
                <i class="fa fa-angle-down"></i>
                <i class="fa fa-angle-up"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="orders.php"><i class="fa fa-shopping-bag"></i> &nbsp;My Orders</a></li>
                <!-- <li class="divider"></li>
                <li><a href="#"><i class="fa fa-address-book-o"></i> &nbsp;My Address</a></li> -->
                <li class="divider"></li>
                <li><a href="profile.php"><i class="fa fa-user-circle-o"></i> &nbsp;My Profile</a></li>
                <li class="divider"></li>
                <li><a href="cart.php?action=logout"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Logout</a></li>
            </ul>
            </li>
            <li>
                <a href="cart.php"><i class="fa fa-shopping-cart" style="color:white"></i> &nbsp;Cart
                <?php

                    $uid = $_SESSION['UserId'];
                    $query = "SELECT COUNT(*) FROM cart where UserId = $uid";
                    $result = mysqli_query($connect, $query);
                    $count = mysqli_fetch_array($result);
                    if ($count[0] > 0):
                    ?>
                    <span class="badge" style="font-size:11px">
                    <?php echo $count[0]; ?>
                    </span>
                <?php endif;?>
                </a>
            </li>
            <li class="dropdown" style="margin-right:5em">
            <a class="dropdown-toggle" href="#">More
                <i class="fa fa-angle-down"></i>
                <i class="fa fa-angle-up"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#" data-toggle="modal" data-target="#myModal_customercare"><i class="fa fa-question-circle"></i> &nbsp;Customer Care</a></li>
                <li class="divider"></li>
                <li><a href="#" data-toggle="modal" data-target="#myModal_aboutus"><i class="fa fa-address-book-o"></i> &nbsp;About Us</a></li>
            </ul>
            </li>
        </ul>
        <ul class="navbar-form ">
            <div class="input-group">
            <input type="text" class="form-control" style="height:30px;width:500px;margin-top:3px" placeholder="Search for products" name="search">
            <div class="input-group-btn">
                <a class="btn btn-default" href="home.php" name="navSearch" style="height:30px;margin-top:3px">
                <i class="glyphicon glyphicon-search"></i>
                </a>
            </div>
            </div>
        </ul>
        </div>
    </nav>

    <form>
            <div class="col-md-1"></div>
            <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel-heading"><span style="font-size:20px">My Cart</span></div>
            <div class="panel-body">
            <?php
                $uid = $_SESSION['UserId'];
                $query = "SELECT c.Id, p.Name, p.Price, p.Image, c.Quantity
                            FROM products p
                            inner join cart c on p.Id = c.ProductId
                            where c.UserId = $uid
                            order by c.Id";
                $result = mysqli_query($connect, $query);
                $count = mysqli_num_rows($result);
                if ($count > 0):
                ?>
            <div class="table-responsive">
            <table class="table">
                <tr>
                    <th width="20%">Product</th>
                    <th width="30%">Product Name</th>
                    <th width="10%" class="text-center">Quantity</th>
                    <th width="20%" class="text-center">Price</th>
                    <th width="15%" class="text-center">Total</th>
                    <th width="5%" class="text-center">Action</th>
                </tr>

            <?php
            $total = 0;
            while ($product = mysqli_fetch_assoc($result)) {
                ?>

                <tr>
                    <td>
                        <img src="data:image/jpeg;base64, <?php echo base64_encode($product['Image']); ?>" name="image" style="width:100px;height:100px" />
                    </td>
                    <td><?php echo $product['Name']; ?></td>
                    <td class="text-center"><?php echo $product['Quantity']; ?></td>
                    <td class="text-center">₹ <?php echo $product['Price']; ?></td>
                    <td class="text-center">₹ <?php echo number_format($product['Quantity'] * $product['Price'], 2); ?></td>
                    <td class="text-center">
                        <a href="cart.php?action=delete&id=<?php echo $product['Id']; ?>">
                            <div class="btn-xs btn-danger" >Remove</div>
                        </a>
                        <!-- <button name="remove" data-id="<?php echo $product['Id']; ?>" class="btn-xs btn-danger">Remove</button> -->
                    <td>
                </tr>
		    <?php
            $total = $total + ($product['Quantity'] * $product['Price']);
            }
            ?>
		   <tr>
				<td colspan="4" class="text-right">Total</td>
				<td class="text-center">₹ <?php echo number_format($total, 2); ?></td>
		   </tr>
		   <tr>
				<td colspan="6">
                    <div class="row">
					<a href="home.php" style="margin-left:10%;width:35%" class="btn btn-info col-md-6"><i class="fa fa-cart-plus" ></i>&nbsp; Continue Shopping </a>
                    <a href="checkout.php" style="margin-left:10%;width:35%" class="btn btn-info col-md-6"><i class="fa fa-credit-card" ></i>&nbsp; Place Order</a>
                    </div>
				</td>
		   </tr>
           </table>
        </div>

                <?php
else: ?>
                <h4 class="text-center">Your Shopping Cart is empty</h4>
                <a href="home.php" style="margin-left:40%;width:20%" class="btn btn-info col-md-6"><i class="fa fa-cart-plus" ></i>&nbsp; Add Items Now </a>
                <?php
endif;?>
            </div>
            </div>
            </div>
            <div class="col-md-1"></div>
    </form>
</body>
</html>