<?php
include("connection.php");
session_start();
$_SESSION['message'] = '';
$db = $con;

?>

<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Scotch Hub</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="bootstrap-3.3.7-dist\css\bootstrap.min.css"/>
		<link rel="stylesheet" href="mainpage.css"/>
		<link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css"/>
		<script src="jquery.min.js"></script>
		<script src="jquery-ui.min.css"></script>
		<script src="bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
		<script src="sweetalert.min.js"></script>
</head>
<body>
	<!-- Login Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Login</h4>
			  </div>
			  <div class="modal-body">
				<form method="post" id="login_form">
					<div class="form-group">
						<label for="loginEmail">Email Id</label>
						<input type="text" class="form-control" Placeholder="Enter Email Id" name="loginEmail" id="loginEmail" required>
					</div>
					<div class="form-group">
						<label for="loginPwd">Password</label>
						<input type="password" class="form-control" Placeholder="Enter Password" name="loginPwd" id="loginPwd" required>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-info" style="width:7em">Login</button>
					</div>
				</form>
				<div class="form-group">
				
					<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModal3">Forgot Password?</a>
					<span style="float:right">Don't have an Account?
					<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">
					 SignUp</a>
					</span>
				</div>
			  </div>
			  <div class="modal-footer">
				<!--<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>-->
			  </div>
			</div>

		  </div>
		</div>

		<!--Forgot Password Modal -->
		<div id="myModal3" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Forgot Password</h4>
			  </div>
			  <div class="modal-body">
				<form method="post" id="forgotPwd_form">
					<div class="form-group">
						<label for="emailid">Email Id</label>
						<input type="email" class="form-control" Placeholder="Enter Email Id" name="emailid" required>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-info" style="width:7em">Submit</button>
					</div>
				</form>
			  </div>
			  <div class="modal-footer">
				<!--<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>-->
			  </div>
			</div>

		  </div>
		</div>

		<!-- Signup Modal -->
		<div id="myModal2" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal2 content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">SignUp</h4>
			  </div>
			  <div class="modal-body">
				<form method="post" id="signup_form" name="signup_form">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" Placeholder="Enter Name" name="name" id="name" required>
					</div>
					<div class="form-group">
						<label for="gender">Gender</label>
						<div class="form-control">
						<input type="radio" name="gender" value="male" id="gender" checked> Male
						<input type="radio" name="gender" value="female" id="gender"> Female
					</div>
					</div>
					<div class="form-group">
						<label for="dob">Date Of Birth</label>
						<input type="date" class="form-control" name="dob" id="dob" required>
					</div>
					<div class="form-group">
						<label for="mno">Mobile Number</label>
						<input type="tel" class="form-control" pattern="[6|7|8|9][0-9]{9}" Placeholder="Enter Mobile Number" name="mno" id="mno" required>
					</div>
					<div class="form-group">
						<label for="eid">Email Id</label>
						<input type="email" class="form-control" Placeholder="Enter Email Id" name="eid" id="eid" required>
					</div>
					<div class="form-group">
						<label for="pwd">Password</label>
						<input type="password" class="form-control" Placeholder="Enter Password" name="pwd" id="pwd" required>
					</div>
					<div class="form-group">
						<label for="cpwd">Confirm Password</label>
						<input type="password" class="form-control" Placeholder="Confirm Password" name="cpwd" id="cpwd" required>
					</div>
					<div class="form-group">
						<button type="submit" name="btnSignup" id="btnSignup" class="btn btn-info"
						style="width:7em">SignUp</button> <!-- data-dismiss="modal" data-toggle="modal" data-target="#myModal" -->
					</div>
				</form>
			  </div>
			  <div class="modal-footer">

			  </div>
			</div>

		  </div>
		</div>

		<!-- more_pop_up -->
		  <!-- cusomet care pop up -->
			<div id="myModal_customercare" class="modal fade" role="dialog">
		  <div class="modal-dialog">

							<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Customer Care</h4>
						</div>
							<div class="modal-body">
								<p><b class="text-danger">
									24/7 care
								</b></p>
								<i class="fa fa-phone"></i> &nbsp;9739751689<br/>
								<i class="fa fa-envelope"></i> &nbsp;bmcgroups.scotchhub@gmail.com
							</div>
						</div>

					</div>

		  </div>
			</div>

			<!-- about us pop up -->
			 <!-- cusomet care pop up -->
			 <div id="myModal_aboutus" class="modal fade" role="dialog">
		  <div class="modal-dialog">

							<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">About Us</h4>
						</div>
						<div class="modal-body">
							<div class="content">
								<p>
									<b>Scotch Hub</b> is an initial sample product of <b>bmch group</b>'s IT sector. 
								</p>
							</div>
						</div>

					</div>

		  </div>
	  	</div>


   		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <!--<img src="scotch-glass.png" style="width:50px;height:80px"/>-->
			  <a class="navbar-brand" style="margin-left:1em" href="mainpage.php">Scotch Hub</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
			  <!--<li class="active"><a href="#">Home</a></li>
			  <li><a href="#">Page 1</a></li>
			  <li><a href="#">Page 2</a></li>-->
			  <li>
				  <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-sign-in" ></i> &nbsp;Login & Signup</a>
			  </li>
			  <li class="dropdown" style="margin-right:5em">
				<a class="dropdown-toggle" href="#">More
					<i class="fa fa-angle-down"></i>
					<i class="fa fa-angle-up"></i>
				</a>
				<ul class="dropdown-menu">
				  <li><a href="#" data-toggle="modal" data-target="#myModal_customercare"><i class="fa fa-question-circle"></i> &nbsp;Customer Care</a></li>
				  <li class="divider"></li>
				  <li><a href="#" data-toggle="modal" data-target="#myModal_aboutus"><i class="fa fa-address-book-o"></i> &nbsp;About Us</a></li>
				</ul>
			  </li>
			</ul>
			<ul class="navbar-form ">
			  <div class="input-group">
				<input type="text" class="form-control" style="height:30px;width:40vw;margin-top:3px" placeholder="Search for products" name="search">
				<div class="input-group-btn">
				  <button class="btn btn-default" type="button" name="navSearch" style="height:30px;margin-top:-13px;position: absolute; left: 0;">
					<i class="glyphicon glyphicon-search"></i>
				  </button>
				</div>
			  </div>
			</ul>
		  </div>
		</nav>

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="mainpage11.jpg" alt="Los Angeles" style="width:100%;height:50%">
        <div class="carousel-caption">
          <h3>Whiskey</h3>
          <p>Whiskey, like a beautiful woman, demands appreciation!</p>
        </div>
      </div>

      <div class="item">
        <img src="mainpage10.jpg" alt="Chicago" style="width:100%;height:50%">
        <div class="carousel-caption">
          <h3>Scotch</h3>
          <p>Love makes the world go round? Not at all. scotch makes it go round twice as fast!</p>
        </div>
      </div>

      <div class="item">
        <img src="mainpage6.jpg" alt="New York" style="width:100%;height:50%">
        <div class="carousel-caption">
          <h3>Wine</h3>
          <p>Everybody likes old scotch but young woman!</p>
        </div>
      </div>

			<div class="item">
        <img src="mainpage4.jpg" alt="New York" style="width:100%;height:50%">
        <div class="carousel-caption">
          <h3>Rum</h3>
          <p>We love the Big Apple!</p>
        </div>
      </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
<script>
	$(document).ready(function(){
		$('#signup_form').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				url: "insertSignupData.php",
				method: "POST",
				data: $('#signup_form').serialize(),
				success: function(data)
				{
					if(data == 'mismatch')
					{
						swal("Password mismatched!", "Please check your password", "warning");
					}
					else if(data == 'inDb')
					{
						swal("EmailId already exists in the Database!", "Please enter different Email Id", "warning");
					}
					else if(data == 'saved')
					{
						$('#signup_form')[0].reset();
						$('#myModal2').modal('hide');
						swal("Successfully Signed Up!", "You can login now", "success");
						$('#myModal').modal('show');
					}
				}
			});
		});

		$('#login_form').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				url: "login.php",
				method: "POST",
				data: $('#login_form').serialize(),
				success: function(data)
				{
					if(data == 'successUser')
					{
						$('#myModal').modal('hide');
						window.location.href = 'home.php';
					}
					else if(data == 'successAdmin')
					{
						$('#myModal').modal('hide');
						window.location.href = 'admin.php';
					}
					else if(data == 'failure')
					{
						swal("Incorrect Email/ Password!", "Please check your Email and Password", "warning");
					}
				}
			});
		});

		$('#forgotPwd_form').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				url: "forgotPwd.php",
				method: "POST",
				data: $('#forgotPwd_form').serialize(),
				success: function(data)
				{
					if(data == 'success')
					{
						$('#forgotPwd_form')[0].reset();
						$('#myModal3').modal('hide');
						swal("New Password has been sent!", "Please check your Email for new Password", "success");
					}
					else if(data == 'nDb')
					{
						$('#forgotPwd_form')[0].reset();
						swal("Email Id does not exist!", "Please signup", "warning");
						$('#myModal3').modal('hide');
						$('#myModal2').modal('show');
					}
					else{
						swal("Message could not be sent!", data, "error");
					}
				}
			});
		});

		$('.modal-content').resizable({
      //alsoResize: ".modal-dialog",
      minHeight: 300,
      minWidth: 300
    });
		$('.modal-dialog').resizable({
      //alsoResize: ".modal-dialog",
      minHeight: 300,
      minWidth: 300
    });
    $('.modal-dialog').draggable();

    $('#myModal').on('show.bs.modal', function() {
      $(this).find('.modal-body').css({
        'max-height': '100%'
      });
    });
		
		$('#myModal2').on('show.bs.modal', function() {
      $(this).find('.modal-body').css({
        'max-height': '100%'
      });
    });

	});
</script>
</body>
</html>
